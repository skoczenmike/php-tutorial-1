<!doctype html>
<html lang="pl">
<head>
  <meta charset="utf-8">
  <title>Doughnuts</title>
</head>
<body>
  <h1>Create order</h1>
<?php

$doughnuts = $_POST['doughnuts'];
$omelette = $_POST['omelette'];

$doughnuts_price = $doughnuts * 0.99;
$omelette_price = $omelette * 2.59;

$summary_price = $doughnuts_price + $omelette_price;

echo<<<END
<h2>Summary order</h2>
<table border="1" cellpadding="10" cellpacing="0">
  <tr>
    <td>doughnuts (0.99 PLN/one piece)</td><td>$doughnuts</td>
  </tr>
  <tr>
    <td>omelette (2.59 PLN/one piece)</td><td>$omelette</td>
  </tr>
  <tr>
    <td>Price</td><td>$summary_price PLN</td>
  </tr>
</table>

END
?>
  <a href="../index.php">
    <input type="button" value="Retry to main site" />
  </a>

</body>
</html>
